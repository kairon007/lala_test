package zuber.android.ui

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import zuber.android.R
import zuber.android.databinding.ItemDeliveryPointBinding
import zuber.android.mvp.model.DeliveryPoint

class DeliveryPointsAdapter(
    private var deliveryPoints: ArrayList<DeliveryPoint>,
    internal var listener: OnItemClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding = DataBindingUtil.inflate<ItemDeliveryPointBinding>(
            layoutInflater!!,
            R.layout.item_delivery_point,
            parent,
            false
        )
        return DeliveryItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as DeliveryItemViewHolder).onBindViewHolder(deliveryPoints!![position])

    }

    override fun getItemCount(): Int {
        return deliveryPoints!!.size
    }

    fun updateList(newlist: ArrayList<DeliveryPoint>) {
        deliveryPoints = newlist
        this.notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int, campaign: DeliveryPoint)
    }

    internal inner class DeliveryItemViewHolder(private var binding: ItemDeliveryPointBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {


        fun onBindViewHolder(item: DeliveryPoint) {
            binding.lytParent.setOnClickListener(this)
            binding.description.text = item.location.address
            binding.name.text = item.description
            Glide.with(itemView.context)
                .load(item.imageUrl)
                .into(binding.image)
        }

        override fun onClick(view: View) {
            listener.onItemClick(view, adapterPosition, deliveryPoints!![adapterPosition])
        }
    }

    fun addData(listItems: ArrayList<DeliveryPoint>) {
        var size = this.deliveryPoints?.size
        if(size==0){ this.notifyDataSetChanged()}
        this.deliveryPoints?.addAll(listItems)
        var sizeNew = this.deliveryPoints?.size
        notifyItemRangeChanged(size!!, sizeNew!!)
    }
}

