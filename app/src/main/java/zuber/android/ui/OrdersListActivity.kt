package zuber.android.ui

import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import zuber.android.R
import zuber.android.databinding.ActivityOrdersListBinding
import zuber.android.mvp.model.DeliveryPoint
import zuber.android.mvp.model.Repository
import zuber.android.mvp.presenter.OrdersListContract
import zuber.android.mvp.presenter.OrdersListPresenter
import zuber.android.utils.PaginationScrollListener

import java.util.ArrayList

class OrdersListActivity : AppCompatActivity(), OrdersListContract.View {
    private lateinit var binding: ActivityOrdersListBinding
    private lateinit var presenter: OrdersListPresenter
    private lateinit var mAdapter: DeliveryPointsAdapter
    var isLoading: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_orders_list)
        presenter = OrdersListPresenter(Repository(this), this)
        init()
        presenter.fetchCachedDeliveries()
    }

    private fun init() {
        binding.content.setHasFixedSize(true)
        var linearLayoutManager = LinearLayoutManager(this)
        binding.content.layoutManager = linearLayoutManager
        binding.content.itemAnimator = DefaultItemAnimator()
        mAdapter = DeliveryPointsAdapter(ArrayList(), object : DeliveryPointsAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int, deliveryPoint: DeliveryPoint) {
                val intent = Intent(this@OrdersListActivity, LocationActivity::class.java)
                intent.putExtra(DELIVERY_POINT, deliveryPoint)
                startActivity(intent)
            }
        })
        binding.content.adapter = mAdapter
        binding.content?.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return presenter.isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                presenter.getMoreDeliveryPoints()
            }
        })
        binding.swipeContainer.setOnRefreshListener {
            presenter.fetchLiveDeliveries()
            binding.swipeContainer.isRefreshing = false
        }
    }

    override fun initDeliveriesUi(deliveryPoints: ArrayList<DeliveryPoint>) {
        mAdapter.updateList(deliveryPoints)
    }

    override fun updateDeliveriesUi(deliveryPoints: ArrayList<DeliveryPoint>) {
        mAdapter.addData(deliveryPoints)
    }

    override fun showError(msg: String) {

        Snackbar.make(binding.root, msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun setProgressVisibility(visibility: Int) {
        binding.progressBar.visibility = visibility
        isLoading = visibility != View.GONE
    }

    companion object {
        var DELIVERY_POINT = "delivery_point"
    }
}
