package zuber.android.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import zuber.android.mvp.model.DeliveryPoint

interface ConsumerApi {

    @GET("deliveries")
    fun getDeliveries(@Query("offset") offset: Int, @Query("limit") limit: Int): Call<ArrayList<DeliveryPoint>>
}
