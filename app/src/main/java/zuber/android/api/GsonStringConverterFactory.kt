package zuber.android.api

import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type


/**
 * This class is used to serialize String without adding extra quotes around them.
 * USED BECAUSE IN THE MULTIPART CASE, WE CANNOT USE JSON!!
 */

class GsonStringConverterFactory : Converter.Factory() {

    override fun requestBodyConverter(type: Type?, parameterAnnotations: Array<Annotation>?, methodAnnotations: Array<Annotation>?, retrofit: Retrofit?): Converter<*, RequestBody>? {
        return if (String::class.java == type) {
            Converter<String, RequestBody> { value -> RequestBody.create(MEDIA_TYPE, value) }
        } else super.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
    }
    companion object {
        private val MEDIA_TYPE = MediaType.parse("text/plain")
    }


}