package zuber.android.api

import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import zuber.android.BuildConfig
import java.io.IOException


/**
 * This class is used to setup and get an implementation of the API endpoints used in our apps.
 * It is a wrapper around Retrofit2 that tries to minimise the creation of the underlying instance.
 * It is the same instance if the same base url is always provided or if the service interface is obtained only with the method that uses the default base url.
 * Otherwise it will only create a new Retrofit instance and interface service when the provided base url changes.
 */
object ApiManager {
    val BASE_URL = "https://mock-api-mobile.dev.lalamove.com/"

    @Volatile
    private var instance: Retrofit? = null
    private var url: String? = null

    @Volatile
    private var consumerApiInstance: ConsumerApi? = null

    /**
     * Checks if an instance of Retrofit already exists with the given base url and otherwise makes one
     *
     * @param baseUrl the base url for Retrofit. If baseUrl is null or empty, Retrofit will throw exceptions.
     * @return a Retrofit instance
     */
    private fun getRetrofit(baseUrl: String): Retrofit? {
        if (url == null || url != baseUrl) {
            synchronized(ApiManager::class.java) {
                createRetrofitInstance(baseUrl)
                url = baseUrl
            }
        } else {
            if (instance == null) {
                synchronized(ApiManager::class.java) {
                    if (instance == null) {
                        createRetrofitInstance(baseUrl)
                    }
                }
            }
        }

        return instance
    }

    /**
     * Instantiates the Retrofit instance
     *
     * @param baseUrl the base url for Retrofit. If baseUrl is null or empty, Retrofit will throw exceptions.
     */
    private fun createRetrofitInstance(baseUrl: String) {
        val httpClient = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
            httpClient.addInterceptor(LoggingInterceptor())
        }

        val gson = GsonBuilder().create()
        instance = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(GsonStringConverterFactory())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build())
            .build()
    }

    /**
     * Get the API service interface for the consumer app using the provided base url
     *
     * @param baseUrl the base url for Retrofit. If baseUrl is null or empty, Retrofit will throw exceptions.
     * @return the API service interface
     */
    fun getConsumerApi(baseUrl: String): ConsumerApi? {

        if (url == null || url != baseUrl) {
            synchronized(ApiManager::class.java) {
                consumerApiInstance = getRetrofit(baseUrl)!!.create(ConsumerApi::class.java)
            }
        } else {
            if (consumerApiInstance == null) {
                synchronized(ApiManager::class.java) {
                    if (consumerApiInstance == null) {
                        consumerApiInstance = getRetrofit(baseUrl)!!.create(ConsumerApi::class.java)
                    }
                }
            }
        }
        return consumerApiInstance
    }


    class LoggingInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()

            val response = chain.proceed(request)

            val bodyString = response.body()!!.string()


            return response.newBuilder()
                .body(ResponseBody.create(response.body()!!.contentType(), bodyString))
                .build()
        }
    }

}
