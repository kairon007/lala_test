package zuber.android.multithreading

import android.os.Handler
import android.os.Looper


import java.util.concurrent.Executors

/**
 * Picked up from Google Arch
 */
class STTaskExecutor : TaskExecutor() {
    private val mLock = Any()
    private val mDiskIO = Executors.newFixedThreadPool(2)

    @Volatile
    private var mMainHandler: Handler? = null

    override val isMainThread: Boolean
        get() = Looper.getMainLooper().thread === Thread.currentThread()

    override fun executeOnDiskIO(runnable: Runnable) {
        mDiskIO.execute(runnable)
    }

    override fun postToMainThread(runnable: Runnable) {
        if (mMainHandler == null) {
            synchronized(mLock) {
                if (mMainHandler == null) {
                    mMainHandler = Handler(Looper.getMainLooper())
                }
            }
        }
        //no inspection ConstantConditions
        mMainHandler!!.post(runnable)
    }
}
