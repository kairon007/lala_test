package zuber.android.multithreading

/**
 * Picked up from Google Arch
 */

abstract class TaskExecutor {

    /**
     * Returns true if the current thread is the main thread, false otherwise.
     *
     * @return true if we are on the main thread, false otherwise.
     */
    abstract val isMainThread: Boolean

    /**
     * Executes the given task in the disk IO thread pool.
     *
     * @param runnable The runnable to run in the disk IO thread pool.
     */
    abstract fun executeOnDiskIO(runnable: Runnable)

    /**
     * Posts the given task to the main thread.
     *
     * @param runnable The runnable to run on the main thread.
     */
    abstract fun postToMainThread(runnable: Runnable)

}

