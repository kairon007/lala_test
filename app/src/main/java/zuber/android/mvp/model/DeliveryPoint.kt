package zuber.android.mvp.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import zuber.android.room.LocationConverter
import zuber.android.room.LocationPoint
import java.io.Serializable

@Entity(tableName = "deliverypoint")
class DeliveryPoint : Serializable {
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0
    lateinit var description: String
    lateinit var imageUrl: String
    @TypeConverters(LocationConverter::class)
    lateinit var location: LocationPoint


}
