package zuber.android.mvp.model

import android.content.Context
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import zuber.android.api.ApiManager
import zuber.android.api.ConsumerApi
import zuber.android.multithreading.STTaskExecutor
import zuber.android.room.AppDatabase

class Repository(context: Context) {
    internal var stTaskExecutor: STTaskExecutor = STTaskExecutor()
    private var consumerApi: ConsumerApi? = null
    internal var appDatabase: AppDatabase = AppDatabase.getAppDatabase(context)

    init {
        this.consumerApi = ApiManager.getConsumerApi(ApiManager.BASE_URL)
    }

    fun refreshDeliveryPoints(offset: Int, limit: Int, deliveriesCallback: DeliveriesCallback) {
        consumerApi!!.getDeliveries(offset, limit).enqueue(object : Callback<ArrayList<DeliveryPoint>> {
            override fun onResponse(
                call: Call<ArrayList<DeliveryPoint>>,
                response: Response<ArrayList<DeliveryPoint>>
            ) {
                stTaskExecutor.executeOnDiskIO(runnable = Runnable {
                    appDatabase.deliveryPointDao().insertAll(response.body()!!)
                })
                deliveriesCallback.onRefreshed(response.body())
            }

            override fun onFailure(call: Call<ArrayList<DeliveryPoint>>, t: Throwable) {
                deliveriesCallback.onError(t)
            }
        })
    }

    fun getCachedDeliveries(deliveriesCallback: DeliveriesCallback) {
        stTaskExecutor.executeOnDiskIO(runnable = Runnable {
            val deliveryPoints = appDatabase.deliveryPointDao().allDeliveryPoints
            stTaskExecutor.postToMainThread(runnable = Runnable {
                if (deliveryPoints.isEmpty()) {
                    deliveriesCallback.onEmpty()
                } else {
                    deliveriesCallback.onCachedResults(ArrayList(deliveryPoints))
                }
            })
        })
    }


    interface DeliveriesCallback {
        fun onCachedResults(deliveryPoints: ArrayList<DeliveryPoint>)

        fun onRefreshed(deliveryPoints: ArrayList<DeliveryPoint>?)

        fun onError(t: Throwable)

        fun onEmpty()
    }
}
