package zuber.android.mvp.presenter


import android.view.View
import zuber.android.mvp.model.DeliveryPoint
import zuber.android.mvp.model.Repository

class OrdersListPresenter(private var repo: Repository, internal var view: OrdersListContract.View) :
    OrdersListContract.Presenter {

    var isLastPage: Boolean = false


    override fun getMoreDeliveryPoints() {
        view.setProgressVisibility(View.VISIBLE)
        CURRENT_OFFSET += DEFAULT_LIMIT
        repo.refreshDeliveryPoints(CURRENT_OFFSET, DEFAULT_LIMIT, object : Repository.DeliveriesCallback {
            override fun onRefreshed(deliveryPoints: ArrayList<DeliveryPoint>?) {
                view.setProgressVisibility(View.GONE)
                view.updateDeliveriesUi(deliveryPoints!!)
            }

            override fun onError(t: Throwable) {
                view.setProgressVisibility(View.GONE)
                view.showError(t.localizedMessage)
            }

            override fun onEmpty() {
                view.setProgressVisibility(View.GONE)
                isLastPage = true
            }

            override fun onCachedResults(deliveryPoints: ArrayList<DeliveryPoint>) {
                view.setProgressVisibility(View.GONE)
                view.initDeliveriesUi(deliveryPoints)
            }
        })
    }

    override fun fetchCachedDeliveries() {
        view.setProgressVisibility(View.VISIBLE)
        repo.getCachedDeliveries(object : Repository.DeliveriesCallback {
            override fun onCachedResults(deliveryPoints: ArrayList<DeliveryPoint>) {
                view.setProgressVisibility(View.GONE)
                view.initDeliveriesUi(deliveryPoints)
            }

            override fun onRefreshed(deliveryPoints: ArrayList<DeliveryPoint>?) {
                view.setProgressVisibility(View.GONE)
                view.updateDeliveriesUi(deliveryPoints!!)
            }

            override fun onError(t: Throwable) {
                view.setProgressVisibility(View.GONE)
                view.showError(t.localizedMessage)
            }

            override fun onEmpty() {
                view.setProgressVisibility(View.GONE)
                fetchLiveDeliveries()
            }
        })
    }

    override fun fetchLiveDeliveries() {
        CURRENT_OFFSET = 0
        view.setProgressVisibility(View.VISIBLE)
        repo.refreshDeliveryPoints(CURRENT_OFFSET, DEFAULT_LIMIT, object : Repository.DeliveriesCallback {
            override fun onCachedResults(deliveryPoints: ArrayList<DeliveryPoint>) {
                view.setProgressVisibility(View.GONE)
                view.initDeliveriesUi(deliveryPoints)
            }

            override fun onRefreshed(deliveryPoints: ArrayList<DeliveryPoint>?) {
                view.setProgressVisibility(View.GONE)
                view.updateDeliveriesUi(deliveryPoints!!)
            }

            override fun onError(t: Throwable) {
                view.setProgressVisibility(View.GONE)
                view.showError(t.localizedMessage)
            }

            override fun onEmpty() {
                view.setProgressVisibility(View.GONE)
                view.showError("Nothing Found")
            }
        })
    }

    companion object {
        const val DEFAULT_LIMIT = 20
        var CURRENT_OFFSET = 0
    }
}
