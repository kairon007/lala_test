package zuber.android.mvp.presenter

import zuber.android.mvp.model.DeliveryPoint
import java.util.ArrayList

interface OrdersListContract {

    interface Presenter {
        fun fetchCachedDeliveries()

        fun fetchLiveDeliveries()

        fun getMoreDeliveryPoints()
    }

    interface View {
        fun initDeliveriesUi(deliveryPoints: ArrayList<DeliveryPoint>)

        fun updateDeliveriesUi(deliveryPoints: ArrayList<DeliveryPoint>)

        fun showError(msg: String)

        fun setProgressVisibility(visibility: Int)

    }

}
