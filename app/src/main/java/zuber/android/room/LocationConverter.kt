package zuber.android.room

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by osx on 10/11/17.
 */

class LocationConverter {
    @TypeConverter
    fun fromString(value: String): LocationPoint? {
        val listType = object : TypeToken<LocationPoint>() {

        }.type
        return Gson().fromJson<LocationPoint>(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: LocationPoint): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}