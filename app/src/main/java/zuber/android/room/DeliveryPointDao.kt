package zuber.android.room

import android.arch.persistence.room.*
import zuber.android.mvp.model.DeliveryPoint


@Dao
interface DeliveryPointDao {

    @get:Query("SELECT * FROM deliverypoint ")
    val allDeliveryPoints: List<DeliveryPoint>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(campaigns: List<DeliveryPoint>)

    @Query("DELETE FROM deliverypoint")
    fun emptyTable()

}
