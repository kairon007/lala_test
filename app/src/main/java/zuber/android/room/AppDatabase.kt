package zuber.android.room


import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import zuber.android.R
import zuber.android.mvp.model.DeliveryPoint


/**
 * Created by osx on 12/10/17.
 */

@Database(entities = [DeliveryPoint::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun deliveryPointDao(): DeliveryPointDao

    companion object {

        private var INSTANCE: AppDatabase? = null


        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, context.getString(R.string.app_name))
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return INSTANCE as AppDatabase
        }
    }


}
