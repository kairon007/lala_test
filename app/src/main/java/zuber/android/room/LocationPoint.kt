package zuber.android.room

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class LocationPoint : Serializable {
    @SerializedName("lat")
    @Expose
    var lat: Double = 0.toDouble()
    @SerializedName("lng")
    @Expose
    var lng: Double = 0.toDouble()
    @SerializedName("address")
    @Expose
    lateinit var address: String
}